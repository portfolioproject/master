// Fill out your copyright notice in the Description page of Project Settings.

#include "PortfolioGame.h"
#include "CharacterController.h"


// Sets default values
ACharacterController::ACharacterController()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("staticMesh"));
	RootComponent = staticMesh;
	cameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("cameraComponent"));
	cameraComponent->AttachTo(RootComponent);
	cameraComponent->SetRelativeLocation(FVector(-400.0f, 0.0f, 400.0f));
	cameraComponent->SetRelativeRotation(FRotator(-45.0f, 0.0f, 0.0f));
	staticMesh->SetSimulatePhysics(true);
	staticMesh->BodyInstance.bLockRotation = true;
	isPast = false;
	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void ACharacterController::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ACharacterController::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	Move(currentSpeed);
}

// Called to bind functionality to input
void ACharacterController::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAxis("MoveForward", this, &ACharacterController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ACharacterController::MoveRight);
	InputComponent->BindAction("SwitchTime", IE_Released, this, &ACharacterController::SwitchTime);

}

void ACharacterController::MoveForward(float value)
{
	float mySpeed = FMath::Clamp(value, -1.0f, 1.0f)*speed;
	currentSpeed.X = mySpeed;
}

void ACharacterController::MoveRight(float value)
{
	float mySpeed = FMath::Clamp(value, -1.0f, 1.0f)*speed;
	currentSpeed.Y = mySpeed;
}

void ACharacterController::SwitchTime()
{
	FVector myPos = GetActorLocation();
	if (isPast) {
		SetActorLocation(myPos - shiftTimeLocation);
	}
	else {
		SetActorLocation(myPos + shiftTimeLocation);
	}
	isPast = !isPast;
}

void ACharacterController::Move(FVector direction)
{
	FVector myPos = GetActorLocation();
	SetActorLocation(myPos + direction);
}


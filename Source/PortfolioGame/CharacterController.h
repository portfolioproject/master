// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "CharacterController.generated.h"

UCLASS()
class PORTFOLIOGAME_API ACharacterController : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACharacterController();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* staticMesh;
	UPROPERTY(EditAnywhere)
		UCameraComponent* cameraComponent;

	UPROPERTY(EditAnywhere)
		float speed;
	UPROPERTY(EditAnywhere)
		FVector shiftTimeLocation;
	UPROPERTY(EditAnywhere)
		bool isPast;
	FVector currentSpeed;

	void MoveForward(float value);
	void MoveRight(float value);
	void SwitchTime();
	void Move(FVector direction);
	
};
